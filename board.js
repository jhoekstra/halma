//grid width and height
var b = 400;
//padding around grid
var p = 10;

//place size
var pl = 39; 

//size of canvas
var c = b + (p*2) + 1;

var canvas = document.getElementById("canvas1");
canvas.width = canvas.height = c;

var ctx = canvas.getContext("2d");

var white_pawn = document.createElement("IMG"); 
var blue_pawn = document.createElement("IMG"); 
var vacant = document.createElement("IMG");
white_pawn.src = "pawn_white.png";
blue_pawn.src = "pawn_blue.png";
vacant.src = "vacant.png";

var playing_colour = 0;
var board = [];
var legal_moves = false;
var move = false;
var reqId = false;
var jumpover = false;

var lightness = 99;
var sign = 1;
var fps = 13; 
var incr = 1;


function setPieces(){
    //make 2d and populate hash
    for (var x=1; x <= 10; x++){
        board[x] = [];

        for (var y=1; y <= 10; y++){
            board[x][y] = 2;
        }
    }
    
    //set whites to 1 an draw
    for (var y=1; y <= 5; y++) {
        for (var x=1; x <= 6-y; x++) {
            board[x][y] = 0; 
            var xy = compCoords(x,y);
            ctx.drawImage(white_pawn, xy[0], xy[1]);
        }
    }

    //set blues to 2 and draw
    for (var y=6; y <= 10; y++) {
        for (var x=16-y; x <=10; x++) {
            board[x][y] = 1;  
            var xy = compCoords(x,y);
            ctx.drawImage(blue_pawn, xy[0], xy[1]);
        }
    }
}

//aux function: board_place -> [x,y]
function compCoords(x, y){
    return [11+40*(x-1), 11+40*(y-1)];
}

//aux function: coords -> x,y boardplaces
function compPlace(x, y){
    x -= 10;
    y -= 10;
    
    x = (x - x % 40) / 40;
    y = (y - y % 40) / 40;

    return [x+1, y+1];
}

//draws the board (background)
function drawBoard(){
    for (var x = 0; x <= b; x += 40) {
        ctx.moveTo(0.5 + x + p, p);
        ctx.lineTo(0.5 + x + p, b + p);
    }

    for (var x = 0; x <= b; x += 40) {
        ctx.moveTo(p, 0.5 + x + p);
        ctx.lineTo(b + p, 0.5 + x + p);
    }

    ctx.strokeStyle = "black";
    ctx.stroke();
}

//from and to are arrays of size 2: [x,y]
function movePiece(from, to) {
    //move piece to new position
    var pawn_colour = board[from[0]][from[1]];
    board[to[0]][to[1]] = pawn_colour;
    board[from[0]][from[1]] = 2;

    //remove draw 
    var xy = compCoords(from[0], from[1]);
    ctx.drawImage(vacant, xy[0], xy[1]);

    //redraw
    var xy = compCoords(to[0], to[1]);
    if (pawn_colour == 0) {
        ctx.drawImage(white_pawn, xy[0], xy[1]);
    }

    else {
        ctx.drawImage(blue_pawn, xy[0], xy[1]);       
    }
}

function handleClick(event){
    var x = event.x;
    var y = event.y;

    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    
    //click not in playboard -> ignore
    if (x < 10 ||
        x > 390 || 
        y < 10 ||
        y > 390) {
        return;
    }

    //set coordinates of pawn in global, determine selected pawn colour
    var coords = compPlace(x, y);
    var x = coords[0];
    var y = coords[1];
    var pawn_colour = board[x][y];

    //no legal moves left on an nth click -> change players
    if (legal_moves && legal_moves.length == 0){
        changeTurn();
        return;
    }

    //first click
    else if (!legal_moves) {

        //selected pawn must be owned
        if (pawn_colour == playing_colour){
            firstClick(coords);
        }

        //need to return when not owned too
        return;
    }
    
    //reselect second click
    else if (!jumpover && pawn_colour == playing_colour){
        undrawLegal();
        firstClick(coords);
        return;
    }

    //second click
    else if (isLegalMove(coords)){
        secondClick(coords);
    }

    //stop jumping over
    else {
        undrawLegal();
        changeTurn();
    }
}

function isLegalMove(coords){
    for (var i=0; i <= legal_moves.length; i++) {
        if (_.isEqual(legal_moves[i], coords)){
            return true;
        }
    }
    return false;
}

function firstClick(coords){
    legal_moves = legalMoves(coords);

    //animate legal moves
    reqId = setInterval(drawLegal, 1000 / fps);

    //set from to current position
    from = coords; 
}


function secondClick(to){
    //remove animation of legal moves
    undrawLegal();

    //move piece
    movePiece(from, to);

    //check if victory has occured
    if (whiteWin()){
        displayVictory(1);
    }

    else if (blueWin()){
        displayVictory(2);
    }

    //compute difference between move and from
    i = Math.abs(to[0] - from[0]);
    j = Math.abs(to[1] - from[1]);

    //update playing colour if was not a jumpover
    if (i < 2 && j < 2){
        changeTurn();
    }

    //select pawn and next click is nth click
    else {
        jumpover = true;
        firstClick(to);
    }
}

function changeTurn(){
    playing_colour += 1;
    playing_colour %= 2;
    jumpover = false;
    legal_moves = false;
}

function whiteWin(){
    //check whites victory
    for (var y=1; y <= 5; y++) {
        for (var x=1; x <= 6-y; x++) {
            if (board[x][y] != 1){
                return false;
            } 
        }
    }
    return true;
}

function blueWin(){
    //check blue victory
    for (var y=6; y <= 10; y++) {
        for (var x=16-y; x <=10; x++) {
            if (board[x][y] != 0){
                return false;
            }
        }
    }
    return true;
}

function displayVictory(victor){
    $("<p/>", {
    id: 'victory',
    href: 'http://www.example.com/',
    text: victor + 'won this round of Halma!'
}).appendTo("body");
}

//draw function of legal moves (looped)
function drawLegal(){
    lightness -= sign*incr; 

    if (lightness < 81 || lightness > 99){
        sign *= -1;
    }

    ctx.fillStyle = 'hsl(240,100%,' + String(lightness) + '%)';
   
    for (var i=0; i < legal_moves.length; i++){
        var move = legal_moves[i];
        var xy = compCoords(move[0], move[1]);
        ctx.fillRect(xy[0], xy[1], pl, pl);    
    }   
}

function undrawLegal(){
    //cancel animation, reset values
    clearInterval(reqId);
    lightness = 99;
    sign = 1;

    //fill potential moves with whitespace
    ctx.fillStyle = 'rgb(255,255,255)';
    for (var i=0; i < legal_moves.length; i++){
        var move = legal_moves[i];
        var xy = compCoords(move[0], move[1]);
        ctx.fillRect(xy[0], xy[1], pl, pl);    
    }  
}

function legalMoves(coords){
    var x = coords[0];
    var y = coords[1];
    var legal_moves = [];
    for (var i=x-1; i <= x+1; i++) {
        for (var j=y-1; j <= y+1; j++) {

            //if illegal index -> skip
            if (!isLegalIndex(i,j)){
                continue;
            }

            //if adjacent place empty...
            if (board[i][j] == 2) {
                
                //if not only jumps needed -> legal move
                if (!jumpover) {
                    legal_moves.push([i,j]);
                }

                continue; 
            }

            //adjacent place is not empty 
            //compute adj+1 place
            var jx = (i-x)+i; 
            var jy = (j-y)+j;

            //again, if illegal index -> skip
            if (!isLegalIndex(jx,jy)) {
                continue;
            }

            //if adj+1 place empty -> legal move
            if (board[jx][jy] == 2){
                legal_moves.push([jx, jy]);
            }
        }
    }

    //#returns [] if no legal_moves
    return legal_moves;
}

function isLegalIndex(i, j) {
    //dont access non-existing board places
    if (i < 1 || 
        i > 10 ||
        j < 1 ||
        j > 10) {
        return false;
        }
    return true;
}

function main(){
    drawBoard();
    setPieces();
    canvas.addEventListener("click", handleClick);
}

window.onload = main;